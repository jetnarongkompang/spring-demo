package com.example.demo;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id = 0;
    private String name;
    private String lastname;
    private String address;
    private String email;
    private String mobile;

    public Users(int id, String name, String lastname, String address, String email, String mobile) {
        this.setId(id);
        this.setName(name);
        this.setLastname(lastname);
        this.setAddress(address);
        this.setEmail(email);
        this.setMobile(mobile);
    }


    public Users(String name, String lastname, String address, String email, String mobile) {
        this.setName(name);
        this.setLastname(lastname);
        this.setAddress(address);
        this.setEmail(email);
        this.setMobile(mobile);
    }

    public Users() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
