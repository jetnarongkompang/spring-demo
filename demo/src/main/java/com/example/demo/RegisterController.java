package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;


@RestController
@RequestMapping("/users")
public class RegisterController {
    @Autowired
    UserRepository userRepository;

    private List<Users> users = new ArrayList<>();
    private final AtomicLong counter = new AtomicLong();
    public RegisterController() {

    }

    @PostMapping("/register")
    public Users create(@RequestBody Users users){

        String name = users.getName();
        String lastname = users.getLastname();
        String address = users.getAddress();
        String email = users.getEmail();
        String mobile = users.getMobile();
        return userRepository.save(new Users(name, lastname,address,email,mobile));
    }

    @GetMapping()
    public List<Users> getUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Users> show(@PathVariable Integer id){
        int userId = Integer.parseInt(String.valueOf(id));
        return userRepository.findById(userId);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable String id){
        int userId = Integer.parseInt(id);
        userRepository.deleteById(userId);
        return true;
    }

}
